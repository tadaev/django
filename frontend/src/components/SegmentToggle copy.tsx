import React from 'react';
import './SegmentToggle.css';

interface SegmentToggleProps {
  segmentLabels: string[];
  activeLabel: string;
  onSegmentClicked: any
}

const SegmentToggle: React.FC<SegmentToggleProps> = ({ onSegmentClicked, activeLabel, segmentLabels }) => {

  return (
    <div className="toggle-container">
      {
        segmentLabels.map(label => {
          return (
            <div key={label} className={`toggle-segment ${activeLabel === label || activeLabel=="" && label==segmentLabels[0]  ? 'active' : ''}`}
                 onClick={() => onSegmentClicked(label)}>
              <div className="toggle-segment-label">
                {label}
              </div>
            </div>
          )
        })
      }
    </div>
  );
};

export default SegmentToggle;
