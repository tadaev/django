import React from 'react';
import './VehicleTypeSelector.css';
import {IonCard} from "@ionic/react";
import {HostUrl} from "../services/ApiService";
import CategoryVehicleType from "../models/CategoryVehicleType";

interface VehicleTypeSelectorProps {
  vehicleTypes: CategoryVehicleType[];
  activeId: number;
  onTypeClicked: (id: number) => void;
}

const VehicleTypeSelector: React.FC<VehicleTypeSelectorProps> = ({ onTypeClicked, activeId, vehicleTypes }) => {

  return (
    <div className="scroll-menu">
      {
        vehicleTypes.map(vtype => {
          const path = vtype.vehicle_type.image.url.split('/');
          const nameImage = path[path.length - 1];
          return (
            <IonCard key={vtype.id}
                     className={`item-type ${activeId === vtype.id ? 'active' : ''}`}
                     onClick={() => onTypeClicked(vtype.vehicle_type.id)}>
              <img src={HostUrl + 'media/img/' + nameImage} />
              <br />
              <p className="description-text">
                {vtype.vehicle_type.name}
              </p>
              <p className="description-text">
                {vtype.category.unit_price}
              </p>
            </IonCard>
          )
        })
      }
    </div>
  );
};

export default VehicleTypeSelector;
