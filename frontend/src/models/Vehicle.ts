import Driver from "./Driver";
import Model from "./Model";

export default class Vehicle {
    constructor(
        public registration_plate: string,
        public driver: Driver,
        public model: Model){}
}