export default class Avatar {
    constructor(
        public id: number,
        public location: string,
        public house: string,
        public street: string,
        public created_at: string,
        public updated_at: string,
        public full_address: string
        ){}
}
