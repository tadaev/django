import Vehicle from "./Vehicle";

export default class Offer {
    constructor(
        public job:number,
        public id: string,
        public amount: number,
        public vehicle: Vehicle){}
}
